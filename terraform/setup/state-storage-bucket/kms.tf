data "aws_caller_identity" "current" {}

resource "aws_kms_key" "state-hofprs-app" {
  description             = "tf-state-hofprs-app-${var.env}"
  enable_key_rotation     = true
  deletion_window_in_days = 7

  lifecycle {
    prevent_destroy = false
  }

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "key-state-policy",
  "Statement": [{
    "Sid": "Allow access for Key Administrators",
    "Effect": "Allow",
    "Principal": {"AWS": [
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Admin",
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/tf-admin",
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Engineer",
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/tf-glrunner-b6e0a732b-master"
    ]},
    "Action": [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion"
    ],
    "Resource": "*"
  },
  {
    "Sid": "Allow use of the key",
    "Effect": "Allow",
    "Principal": {"AWS": ${jsonencode(concat(list(format("arn:aws:iam::%s:root", data.aws_caller_identity.current.account_id)),var.role-arns))}},
    "Action": [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ],
    "Resource": "*"
  }]
}
EOF
}

resource "aws_kms_alias" "state" {
  name          = "alias/tf-state-hofprs-app-${var.env}"
  target_key_id = "${aws_kms_key.state-hofprs-app.key_id}"
}
