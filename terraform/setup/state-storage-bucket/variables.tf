variable env {
  description = "unique environment identifier"
  type        = "string"
  default     = "dev"
}

variable region {
  default = "ap-south-1"
}

variable role-arns {
  description = "list of role arns that will have access to s3 and kms"
  type        = "list"
  default     = ["arn:aws:iam::424506183973:role/tf-glrunner-b6e0a732b-master", "arn:aws:iam::424506183973:role/Admin", "arn:aws:iam::424506183973:role/Engineer"]
}
