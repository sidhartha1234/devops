### Terraform

#### Single product
For a single product a simple Terraform repository setup is fine.

#### Multiple products
When delivering multiple products from one monorepo via the gitlab runner we recommend splitting up the terraform state by (for example) product. In the repository, this would mean adding terraform/main/example-app2 and so on. The different states can be referenced with terraform remote state. Developing on featurebranches while still being able to reference the state of other products that you have in master can be done with terraform environments.
Read more [here](https://www.terraform.io/docs/state/environments.html).

#### Deploying over dt and ap
In the folder op your product, you can create 2 .tfvars files. Call one something like dev.tfvars and the other one like prod.tfvars.
In GitlabCI you can create 2 jobs: one that will run only on fb_ commits (featurebranches). In the terraform plan command, specify the dev.tfvars as the variable file. For pushes to master, you can execute the terraform plan with the prd.tfvars. Ofcourse it's also possible to specify different s3 backends per environment if you want to separate the state files. 
