provider "aws" {
  #region = "${var.aws_region}"
  region                  = "eu-west-1"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
}

terraform {
  backend "s3" {
    bucket     = "tf-state-storage-hof-prs-app-dev"
    key        = "dev/terraform.tfstate"
    region     = "eu-west-1"
    encrypt    = true
    kms_key_id = "alias/tf-state-hofprs-app-dev"
  }
}
