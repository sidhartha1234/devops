### Packer

#### Getting started
Follow this guide for a basic understanding of Packer: https://www.packer.io/intro/getting-started/build-image.html

#### The implementation
We use the pipeline to execute Packer with the following commands:
`packer build -machine-readable ${CI_PROJECT_DIR}/packer/example-app.json  | tee build.log`
This executes the json packer configuration and logs all the output in the build.log
`output_ami_id=$(grep 'artifact,0,id' build.log | cut -d, -f6 | cut -d: -f2)`
This greps the AMI ID
`echo $output_ami_id > ami.log`
This puts the AMI ID in a file called ami.log. You could move this file to an S3 backend in order to fetch it and reuse it later in the pipeline.
`aws s3 cp ${CI_PROJECT_DIR}/packer/packer-manifest.json s3://tf-gitlabci-artifacts-hofprs-app-dev/packer-manifest-json/``
This moves the manifest.json to S3. You can also use this to fetch build information later on in the pipeline, but it is also good practice to log information regarding your builds and keep the history.

#### Description of JSON configuration file

##### Variables
In "variables" section we load the variables defined in GitLabCI pipeline.

- `ansible_path`: path to Ansible roles folder
- `playbook_path`: path to Ansible playbooks folder
- `subnet_id`: subnet where Packer can provision the builder instance
- `vpc_id`: vpc where Packer can provision the builder instance
- `iam_instance_profile`: iam instance profile for Packer builder

##### Builders
In "builders" we define the parameters of Packer builders. We will not explain all used parameters here, as detailed description is available in packer documentation. Instead, we will explain only nn specific settings.

- `security_group_ids`: ids of security groups for linux management, forward proxy access and SG allowing SSH from GitLab runner to Packer Builder.
- `source_ami_filter`: this configuration always gets the most recent centos7 base build provided by NN AWS Team
- `ami_name`: name your final ami and use timestamp suffix to avoid AMIs with conflicting names

##### Provisioners
Type of Packer provisioner (e.g. "ansible" in our case) and it's configuration. All you need to do here is to specify your application specific ansible playbook.

##### Post-processors
Generate Packer manifest file with AMI id and other details.  
This part is not mandatory. In our example, instead of parsing builded AMI and using it as a variable in Terraform, we let Terraform to do AWS API call to get the latest AMI with specific name prefix (defined in `ami_name` variable above; so for example: AWS give me the latest AMI with the name beggining with "example-app-").
