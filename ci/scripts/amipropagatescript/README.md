## AMI propagate service script explained

### Design
The design is that of decentralized ami propagation capability. This means each account will get a KMS key for AMI propagation and a role that can access these keys across accounts.
The trick is that this role has more permissions on the own KMS propagation key than on other KMS propagation keys.
Also, this role will be able to do the HEAD replacement.

### How to use
For the necessary steps for propagation refer to this excellent guide: https://aws.amazon.com/blogs/aws/new-cross-account-copying-of-encrypted-ebs-snapshots/
For the steps to create a working AMI refer to the tab: Clarification 'HEAD' replacement


###### Clarification snapshot vs AMI
An Amazon Machine Image (AMI) is a master image for the creation of virtual servers (known as EC2 instances) in the Amazon Web Services (AWS) environment.
The machine images are like templates that are configured with an operating system and other software, which determine the user’s operating environment. AMI types are categorized according to region, operating system, system architecture (32- or 64-bit), launch permissions and whether they are backed by Amazon EBS or backed by the instance store.

Each AMI includes a template for the root volume required for a particular type of instance; a typical example might contain an operating system, an application server and applications. Permissions are controlled to constrain AMIs for instance launches to the appropriate AWS accounts. A block device mapping ensures that the correct volumes are attached to the launched instance.

A snapshot is a point in time backup of such an EBS Volume.

###### Clarification 'HEAD' replacement
At this moment, for moving AMIs between 2 accounts there are 2 options. Share-AMI and copy snapshot. With share AMI, the whole AMI is made visible for other accounts. This does not take encryption into consideration, a big reason as why market place AMIs are unencrypted by default.
The other option is copy-snapshot. As explained above, an AMI consists partly of an EBS volume. Of this volume, you can make a snapshot that you encrypt with a key that is accessible for other accounts. Also, you will need to add share permissions to this snapshot. In the other account, an IAM user / role with the proper permissions can now copy this snapshot, in the process reencrypting it with a new KMS key that is private.
This will result in a new snapshot. From this snapshot, you can create a volume, and you can start launching instances with this volume. The problem is however, that you lost all the meta information captured in the AMI, for example licensing for RedHat or Windows.
This is solved by doing 'HEAD' replacement.
Spin up a marketplace AMI with your product. Switch the EBS volume with the encrypted corrent EBS volume that you made from the snapshot. From this instance, create a new AMI in your account.

###### Clarification propagate script
The propagate script the AWS-Team made takes care of this whole process from beginning to end. In our test, we use it with 2 AMI propagate keys. We recommend you to pass variables to this script so that you do the reencryption with your own KMS key to add another layer of protection to prevent other accounts from accessing your EBS volumes. Use the AMI propagate keys to share snapshots you want to propagate.
If you do not have specific access to another KMS key please reencrypt from the AMI propagate KMS key after the script is finished.

Use the head_replacement_ami_override parameter in the script to specify the imageID of the AMI you want to use for head replacement. You can use this to point to your latest AMI that works in the target account.

### Components
Propagate kms key in each account with a specific KMS key policy
An IAM role that is assumable across roles in NN accounts
A risk description
ADR.

### Risk
Assuming the role means access for other BUs to the KMS key for ami propagation and gives other NN accounts the possibility to spin up instances or terminate them. We will isolate this as much as possible but always after receiving an AMI reencrypt it with your own managed ebs volume KMS key.
We will remove this ability from the IAM propagate role when BUs are able to create their own IAM roles and policies in their namespace.

In order to reduce the risk on aws ec2 powerful actions, like runinstances and terminate instances we have the following granular permissions and conditions in place:
1. ec2:instanceprofile = null
2. ec2:encrypt must be true NOTE: this can only be set when all ami propagates from the AWS TEAM are done with the AMI propagate key.
3. ec2:resourcetag must be set with key = IACCEPTTHATTHISTAGWILLDESTROYMYINSTANCESANDIACCEPTRESPONSIBILITYFORSETTINGIT value = true
4. ec2:region = eu-west-1
5. ec2:owner is the target account id.

This enforces runinstances to use current available AMIs in the account. You can use a base AMI of the AWS-Team for head replacement. Pass the AMI id as the optional 8th variable to the propagate script.
Also, this reduces terminate instances to just be able to terminate instances who have this ridiculous tag set.

Finally, ec2:deletesnapshot does not support resource level granular permissions and conditions in the IAM policy.
Refer to http://docs.aws.amazon.com/AWSEC2/latest/APIReference/ec2-api-permissions.html#instance for more information.

Fetching a latest AMI from the AWS-Team can be done automatically using something like:
`aws ec2 describe-images --filters "Name=name,Values=centos_7_base_release_*" --region eu-west-1 --query 'Images[*].[ImageId,CreationDate]' --output text | sort -k2 -r | head -n1 | cut -c 1-12`

### Risk mitigation
Ami propagate role can:
- only run instances without instance role
- only run instances in eu-west-1
- only run instances from marketplace

- only terminate instances from marketplace
- only terminate instances without instance role
- only terminate instances in eu-west-1
- only terminate instances with name temp prop instance

### Script example
The script can be found in the [example project](https://vcs.aws.insim.biz/aws-team/example-project/tree/master) under ci/scripts/propagate.sh
